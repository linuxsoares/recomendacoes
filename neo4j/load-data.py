from neo4jrestclient.client import GraphDatabase

data = """
// Create pontos turisticos
CREATE (AquaRio:PontoTuristico{name:'AquaRio',address:'Av. Rodrigues Alves 379',zipcode:'',latitude:'-22.895.843',longitude:'-431.809.964'})
CREATE (ArcodoTeles:PontoTuristico{name:'ArcodoTeles',address:'Praca Quinze de Novembro 34 A',zipcode:'20010-010',latitude:'-229.027.395',longitude:'-431.768.956'})
CREATE (ArcosdaLapa:PontoTuristico{name:'ArcosdaLapa',address:'',zipcode:'',latitude:'-229.159.704',longitude:'-431.798.529'})
CREATE (AterrodoFlamengo:PontoTuristico{name:'AterrodoFlamengo',address:'Av. Infante Dom Henrique 10',zipcode:'',latitude:'-229.293.666',longitude:'-431.746.487'})
CREATE (BarTombadoAdegaDaVelha:PontoTuristico{name:'BarTombadoAdegaDaVelha',address:'R. Paulo Barreto 25',zipcode:'22280-010',latitude:'-229.527.465',longitude:'-43.189.476'})
CREATE (BarTombadoAdegaFlorDeCoimbra:PontoTuristico{name:'BarTombadoAdegaFlorDeCoimbra',address:'Rua Teotonio Regadas 34',zipcode:'20021-360',latitude:'-229.150.872',longitude:'-43.178.928'})
CREATE (BarTombadoAdegaPerola:PontoTuristico{name:'BarTombadoAdegaPerola',address:'Rua Siqueira Campos 138',zipcode:'22031-010',latitude:'-229.661.676',longitude:'-431.873.367'})
CREATE (BarTombadoArmazemCardosao:PontoTuristico{name:'BarTombadoArmazemCardosao',address:'Rua Cardoso Junior 312',zipcode:'22245-000',latitude:'-22.938.105',longitude:'-431.885.728'})
CREATE (BarTombadoArmazemDoSenado:PontoTuristico{name:'BarTombadoArmazemDoSenado',address:'Av Gomes Freire 256',zipcode:'20231-013',latitude:'-22.909.181',longitude:'-431.868.347'})
CREATE (BarTombadoArmazemSaoThiagoBarDoGomez:PontoTuristico{name:'BarTombadoArmazemSaoThiagoBarDoGomez',address:'Rua aurea 26',zipcode:'20240-210',latitude:'-229.213.177',longitude:'-431.895.082'})
CREATE (BarTombadoBarBrasil:PontoTuristico{name:'BarTombadoBarBrasil',address:'Av Geremario Dantas 669 B',zipcode:'22743-010',latitude:'-229.285.187',longitude:'-433.545.988'})
CREATE (BarTombadoBarDaDonaMariaCafeEBarBrotinh:PontoTuristico{name:'BarTombadoBarDaDonaMariaCafeEBarBrotinh',address:'Rua Garibaldi 13',zipcode:'20511-330',latitude:'-229.353.748',longitude:'-432.444.492'})
CREATE (BarTombadoBarDoAdonis:PontoTuristico{name:'BarTombadoBarDoAdonis',address:'Rua Sao Luiz Gonzaga 2156A',zipcode:'20910-062',latitude:'-228.939.954',longitude:'-432.413.173'})
CREATE (BarTombadoBarLagoa:PontoTuristico{name:'BarTombadoBarLagoa',address:'Av Epitacio Pessoa 1674',zipcode:'22410-090',latitude:'-229.800.583',longitude:'-43.203.348'})
CREATE (BarTombadoBarLuiz:PontoTuristico{name:'BarTombadoBarLuiz',address:'Rua da Carioca 39',zipcode:'20050-008',latitude:'-229.066.827',longitude:'-431.823.171'})
CREATE (BarTombadoBarUrca:PontoTuristico{name:'BarTombadoBarUrca',address:'Rua Candido Gafree 205',zipcode:'22291-080',latitude:'-229.435.718',longitude:'-431.602.078'})
CREATE (BarTombadoBipBip:PontoTuristico{name:'BarTombadoBipBip',address:'Rua Alm Goncalves 50 Loja D',zipcode:'22060-040',latitude:'-229.798.128',longitude:'-431.905.087'})
CREATE (BarTombadoBotequimDoJoia:PontoTuristico{name:'BarTombadoBotequimDoJoia',address:'Rua Julia Lopes de Almeida 26',zipcode:'20080-060',latitude:'-229.004.977',longitude:'-431.862.115'})
CREATE (BarTombadoCafeEBarLisbela:PontoTuristico{name:'BarTombadoCafeEBarLisbela',address:'Rua Conde Azambuja 881',zipcode:'',latitude:'-228.854.557',longitude:'-432.652.081'})
CREATE (BarTombadoCasaDaCachaca:PontoTuristico{name:'BarTombadoCasaDaCachaca',address:'Av Mem de Sa 100',zipcode:'20230-150',latitude:'-22.912.963',longitude:'-431.824.369'})
CREATE (BarTombadoCasaPaladino:PontoTuristico{name:'BarTombadoCasaPaladino',address:'Rua Uruguaiana 224 / 226',zipcode:'20050-092',latitude:'-229.010.649',longitude:'-43.184.266'})
CREATE (BarTombadoCasaVilarino:PontoTuristico{name:'BarTombadoCasaVilarino',address:'Av Calogeras 6',zipcode:'20030-070',latitude:'-22.911.201',longitude:'-43.172.881'})
CREATE (BarTombadoCervantes:PontoTuristico{name:'BarTombadoCervantes',address:'Av Prado Junior 335 - Loja B',zipcode:'23550-101',latitude:'-22.921.354',longitude:'-437.035.775'})
CREATE (BarTombadoCosmopolita:PontoTuristico{name:'BarTombadoCosmopolita',address:'Travessa do Mosqueira 04',zipcode:'20230-180',latitude:'-229.138.298',longitude:'-431.788.809'})
CREATE (BarTombadoJobi:PontoTuristico{name:'BarTombadoJobi',address:'Av Ataulfo de Paiva 1166 B',zipcode:'22440-035',latitude:'-229.852.609',longitude:'-43.226.989'})
CREATE (BarTombadoLamas:PontoTuristico{name:'BarTombadoLamas',address:'Rua Marquês de Abrantes 18',zipcode:'22230-060',latitude:'-229.338.978',longitude:'-431.774.303'})
CREATE (BarTombadoNovaCapela:PontoTuristico{name:'BarTombadoNovaCapela',address:'Av  Mem de Sa 96',zipcode:'20230-152',latitude:'-229.129.289',longitude:'-431.823.262'})
CREATE (BarTombadoPavaoAzul:PontoTuristico{name:'BarTombadoPavaoAzul',address:'Rua Barata Ribeiro 208 - Loja D',zipcode:'22040-002',latitude:'-229.653.937',longitude:'-431.832.218'})
CREATE (BarTombadoRestauranteSalete:PontoTuristico{name:'BarTombadoRestauranteSalete',address:'Rua Afonso Pena 189',zipcode:'20270-244',latitude:'-229.152.076',longitude:'-432.201.214'})
CREATE (BibliotecaNacional:PontoTuristico{name:'BibliotecaNacional',address:'Av. Rio Branco 219',zipcode:'20040-009',latitude:'-229.109.366',longitude:'-431.776.549'})
CREATE (BibliotecaParqueEstadual:PontoTuristico{name:'BibliotecaParqueEstadual',address:'Av. Presidente Vargas 1261',zipcode:'20071-004',latitude:'-229.043.952',longitude:'-431.894.892'})
CREATE (BosquedaBarra:PontoTuristico{name:'BosquedaBarra',address:'Av. das Americas 6000',zipcode:'',latitude:'-229.976.912',longitude:'-433.715.302'})
CREATE (CADEG:PontoTuristico{name:'CADEG',address:'Rua Capitao Felix 110',zipcode:'20920-310',latitude:'-228.950.621',longitude:'-432.375.707'})
CREATE (CRAB:PontoTuristico{name:'CRAB',address:'Praca Tiradentes 67',zipcode:'20060-070',latitude:'-229.068.746',longitude:'-431.841.523'})
CREATE (CaisdoValongo:PontoTuristico{name:'CaisdoValongo',address:'Av. Barao de Tefe s/n - Saude',zipcode:'20220-460',latitude:'-22.897.871',longitude:'-431.893.377'})
CREATE (CaminhoNiemyer:PontoTuristico{name:'CaminhoNiemyer',address:'Rua Prof Plinio Leite 86 - Centro -  Niteroi - State of  Rio de Janeiro',zipcode:'',latitude:'-228.880.681',longitude:'-431.298.471'})
CREATE (CampoOlimpicodeGolfe:PontoTuristico{name:'CampoOlimpicodeGolfe',address:'Av das Americas 10.001',zipcode:'22793-082',latitude:'-230.049.967',longitude:'-434.091.335'})
CREATE (CampodeSantana:PontoTuristico{name:'CampodeSantana',address:'Praca da Republica S/N',zipcode:'20211-351',latitude:'-229.063.839',longitude:'-431.907.998'})
CREATE (CasaFrancaBrasil:PontoTuristico{name:'CasaFrancaBrasil',address:'Rua Visc de Itaborai 78',zipcode:'20010-060',latitude:'-229.005.547',longitude:'-43.178.131'})
CREATE (CasaHistoricadeDeodoro:PontoTuristico{name:'CasaHistoricadeDeodoro',address:'Praca da Republica 197',zipcode:'20211-350',latitude:'-229.056.347',longitude:'-431.930.737'})
CREATE (CasadoJongo:PontoTuristico{name:'CasadoJongo',address:'Rua Compositor Silas de Oliveira 101',zipcode:'21360-340',latitude:'-228.650.515',longitude:'-433.325.605'})
CREATE (CentroCulturalBancodoBrasil:PontoTuristico{name:'CentroCulturalBancodoBrasil',address:'Rua Primeiro de Marco 66',zipcode:'20010-000',latitude:'-229.007.953',longitude:'-431.786.137'})
CREATE (CentroCulturalJoseBonifacio:PontoTuristico{name:'CentroCulturalJoseBonifacio',address:'Rua Pedro Ernesto 80 - Gamboa',zipcode:'',latitude:'-228.962.416',longitude:'-431.965.481'})
CREATE (CentroCulturalJusticaFederal:PontoTuristico{name:'CentroCulturalJusticaFederal',address:'Av Rio Branco 241',zipcode:'20040-009',latitude:'-22.910.505',longitude:'-431.775.205'})
CREATE (CentroCulturaldosCorreios:PontoTuristico{name:'CentroCulturaldosCorreios',address:'Rua Visc de Itaborai 20',zipcode:'20010-060',latitude:'-229.012.672',longitude:'-43.178.015'})
CREATE (CentrodeMemoriaRobertoSilveira:PontoTuristico{name:'CentrodeMemoriaRobertoSilveira',address:'Av Plinio Leite s/n - Centro de Niteroi',zipcode:'',latitude:'-228.881.371',longitude:'-431.294.318'})
CREATE (ChacaraTropical:PontoTuristico{name:'ChacaraTropical',address:'Rua Dom Rosalvo Costa Rêgo 420 - Itanhanga',zipcode:'22641-040',latitude:'-229.923.406',longitude:'-433.044.845'})
CREATE (CidadedasArtes:PontoTuristico{name:'CidadedasArtes',address:'Av das Americas 5300',zipcode:'22793-080',latitude:'-229.991.718',longitude:'-433.680.364'})
CREATE (CinelandiaPracaFloriano:PontoTuristico{name:'CinelandiaPracaFloriano',address:'',zipcode:'',latitude:'-229.108.113',longitude:'-431.779.259'})
CREATE (CristoRedentor:PontoTuristico{name:'CristoRedentor',address:'Parque Nacional da Tijuca - Alto da Boa Vista',zipcode:'',latitude:'-22.951.911',longitude:'-432.126.759'})
CREATE (EscadariaSelaron:PontoTuristico{name:'EscadariaSelaron',address:'Rua Joaquim Silva S/N',zipcode:'20241-110',latitude:'-229.153.056',longitude:'-431.813.925'})
CREATE (EspacoCienciaViva:PontoTuristico{name:'EspacoCienciaViva',address:'Av Heitor Beltrao 321 - Esquina da Rua Pareto',zipcode:'20550-00',latitude:'-22.922.334',longitude:'-432.318.807'})
CREATE (EstacaoHidroviariadeCharitas:PontoTuristico{name:'EstacaoHidroviariadeCharitas',address:'Av Quintino Bocaiuva s/n- Charitas - Niteroi',zipcode:'',latitude:'-229.297.447',longitude:'-430.992.183'})
CREATE (EstradadasPaineiras:PontoTuristico{name:'EstradadasPaineiras',address:'',zipcode:'',latitude:'-22.947.559',longitude:'-432.116.986'})
CREATE (EstadioOlimpicoEngenhao:PontoTuristico{name:'EstadioOlimpicoEngenhao',address:'Rua Jose dos Reis 425',zipcode:'20770-062',latitude:'-228.932.699',longitude:'-432.945.015'})
CREATE (FeiradasYabas:PontoTuristico{name:'FeiradasYabas',address:'Praca Paulo da Portela s/n',zipcode:'',latitude:'-228.676.935',longitude:'-433.467.147'})
CREATE (FeiradeSaoCristovao:PontoTuristico{name:'FeiradeSaoCristovao',address:'Campo de Sao Cristovao s/n',zipcode:'20291-440',latitude:'-2.289.758',longitude:'-432.226.662'})
CREATE (FlorestadaTijuca:PontoTuristico{name:'FlorestadaTijuca',address:'',zipcode:'',latitude:'-229.577.543',longitude:'-433.322.062'})
CREATE (FortalezaNossaSenhoradaConceicao:PontoTuristico{name:'FortalezaNossaSenhoradaConceicao',address:'Rua Major Daemon 81 - Morro da Conceicao',zipcode:'',latitude:'-229.574.074',longitude:'-433.322.063'})
CREATE (FortalezadeSaoJoao:PontoTuristico{name:'FortalezadeSaoJoao',address:'Av Joao Luis Alves s/n',zipcode:'',latitude:'-229.574.074',longitude:'-433.322.063'})
CREATE (ForteDuquedeCaxiasPedradoLeme:PontoTuristico{name:'ForteDuquedeCaxiasPedradoLeme',address:'Praca Almirante Julio de Noronha s/n',zipcode:'',latitude:'-22.963.186',longitude:'-431.645.727'})
CREATE (FortedeCopacabana:PontoTuristico{name:'FortedeCopacabana',address:'Praca Coronel Eugênio Franco 1 - Posto 6',zipcode:'',latitude:'-229.574.074',longitude:'-433.322.063'})
CREATE (FundacaoCasadeRuiBarbosa:PontoTuristico{name:'FundacaoCasadeRuiBarbosa',address:'Rua Sao Clemente 134',zipcode:'22260-000',latitude:'-229.487.716',longitude:'-431.892.801'})
CREATE (FundacaoOscarNiemeyer:PontoTuristico{name:'FundacaoOscarNiemeyer',address:'Av  Plinio Leite s/n - (caminho Nyemeier) Centro de Niteroi',zipcode:'',latitude:'-229.168.955',longitude:'-432.127.975'})
CREATE (FabricaBhering:PontoTuristico{name:'FabricaBhering',address:'Rua Orestes 28 - Santo Cristo',zipcode:'',latitude:'-229.010.727',longitude:'-432.055.709'})
CREATE (GRESImperatrizLeopoldinense:PontoTuristico{name:'GRESImperatrizLeopoldinense',address:'Rua Professor Lace 235',zipcode:'21060-120',latitude:'-228.562.058',longitude:'-432.649.876'})
CREATE (GRESImperioSerrano:PontoTuristico{name:'GRESImperioSerrano',address:'Avenida Edgard Romero 114',zipcode:'',latitude:'-228.739.988',longitude:'-433.389.455'})
CREATE (GRESMangueira:PontoTuristico{name:'GRESMangueira',address:'Rua Visconde de Niteroi 1072',zipcode:'',latitude:'-229.036.081',longitude:'-432.427.941'})
CREATE (GRESPortela:PontoTuristico{name:'GRESPortela',address:'Rua Clara Nunes 81',zipcode:'21351-110',latitude:'-228.963.164',longitude:'-432.006.145'})
CREATE (GRESSalgueiro:PontoTuristico{name:'GRESSalgueiro',address:'Rua Silva Teles 104',zipcode:'20540-150',latitude:'-229.217.928',longitude:'-43.245.728'})
CREATE (GRESSaoClemente:PontoTuristico{name:'GRESSaoClemente',address:'Av Pres Vargas 3102',zipcode:'20210-031',latitude:'-229.090.916',longitude:'-432.071.294'})
CREATE (GRESVilaIsabel:PontoTuristico{name:'GRESVilaIsabel',address:'Boulevard 28 de Setembro 382',zipcode:'20551-031',latitude:'-229.161.515',longitude:'-432.511.341'})
CREATE (GalpaoGamboa:PontoTuristico{name:'GalpaoGamboa',address:'Rua da Gamboa 279 - Gamboa',zipcode:'',latitude:'-228.968.822',longitude:'-432.016.204'})
CREATE (HortoFlorestal:PontoTuristico{name:'HortoFlorestal',address:'Rua Pacheco Leao 2040',zipcode:'22460-030',latitude:'-22.968.071',longitude:'-432.420.697'})
CREATE (IlhaFiscal:PontoTuristico{name:'IlhaFiscal',address:'Avenida Alfredo Agache s/n',zipcode:'20040-020',latitude:'-22.896.924',longitude:'-431.756.992'})
CREATE (IlhadePaqueta:PontoTuristico{name:'IlhadePaqueta',address:'Praca Quinze de Novembro',zipcode:'',latitude:'-227.594.197',longitude:'-431.168.597'})
CREATE (JardimBotanicoTP:PontoTuristico{name:'JardimBotanicoTP',address:'Rua Jardim Botanico 1008',zipcode:'',latitude:'-229.643.022',longitude:'-432.409.623'})
CREATE (JardinsSuspensosdoValongo:PontoTuristico{name:'JardinsSuspensosdoValongo',address:'Rua Camerino',zipcode:'20080-011',latitude:'-228.985.599',longitude:'-431.897.891'})
CREATE (JockeyClub:PontoTuristico{name:'JockeyClub',address:'Praca Santos Dumont 31',zipcode:'22470-060',latitude:'-229.745.449',longitude:'-432.235.009'})
CREATE (LagoaRodrigodeFreitas:PontoTuristico{name:'LagoaRodrigodeFreitas',address:'Lagoa Rodrigo de Freitas',zipcode:'',latitude:'-229.715.696',longitude:'-432.178.451'})
CREATE (LargodaCariocaCaixaCultural:PontoTuristico{name:'LargodaCariocaCaixaCultural',address:'Av Alm. Barroso 25',zipcode:'20031-003',latitude:'-229.078.426',longitude:'-431.790.007'})
CREATE (MACMuseudeArteContemporanea:PontoTuristico{name:'MACMuseudeArteContemporanea',address:'Av General Milton Tavares Souza- Mirante da Boa Viagem - Boa Viagem - Niteroi',zipcode:'',latitude:'-229.078.308',longitude:'-431.280.557'})
CREATE (MaracanaTP:PontoTuristico{name:'MaracanaTP',address:'Av Pres Castelo Branco s/n',zipcode:'20271-130',latitude:'-229.121.039',longitude:'-432.323.445'})
CREATE (MarinadaGloria:PontoTuristico{name:'MarinadaGloria',address:'Avenida Infante Dom Henrique S/N',zipcode:'20021-140',latitude:'-22.920.235',longitude:'-431.720.617'})
CREATE (MemorialdosPretosNovos:PontoTuristico{name:'MemorialdosPretosNovos',address:'Rua Pedro Ernesto 34 - Gamboa',zipcode:'',latitude:'-228.959.887',longitude:'-431.950.678'})
CREATE (MercadaodeMadureira:PontoTuristico{name:'MercadaodeMadureira',address:'Av Ministro Edgard Romero 239',zipcode:'',latitude:'-228.706.443',longitude:'-433.385.457'})
CREATE (MesadoImperador:PontoTuristico{name:'MesadoImperador',address:'',zipcode:'',latitude:'-229.703.403',longitude:'-432.600.184'})
CREATE (MiranteDonaMarta:PontoTuristico{name:'MiranteDonaMarta',address:'',zipcode:'',latitude:'-22.944.995',longitude:'-431.986.183'})
CREATE (MirantedaPrainha:PontoTuristico{name:'MirantedaPrainha',address:'Av. Estado da Guanabara - Barra da Tijuca',zipcode:'',latitude:'-230.412.991',longitude:'-435.083.381'})
CREATE (MirantedoLeblon:PontoTuristico{name:'MirantedoLeblon',address:'Rua Aperana - Alto Leblon - entrada do Parque Penhasco Dois Irmaos',zipcode:'',latitude:'-22.989.972',longitude:'-432.295.286'})
CREATE (MirantedoPasmado:PontoTuristico{name:'MirantedoPasmado',address:'Ladeira da Rua General Severiano - subindo a rampa do Tunel do Pasmado',zipcode:'',latitude:'-229.514.189',longitude:'-431.803.387'})
CREATE (MonumentoEstaciodeSa:PontoTuristico{name:'MonumentoEstaciodeSa',address:'Aterro do Flamengo entre as praias de Botafogo e Flamengo',zipcode:'',latitude:'-229.399.273',longitude:'-431.719.377'})
CREATE (MorroDoisIrmaos:PontoTuristico{name:'MorroDoisIrmaos',address:'Vidigal',zipcode:'22450-242',latitude:'-22.990.758',longitude:'-432.420.117'})
CREATE (MorrodaUrca:PontoTuristico{name:'MorrodaUrca',address:'',zipcode:'',latitude:'-229.505.507',longitude:'-431.730.231'})
CREATE (MuseuAerospacial:PontoTuristico{name:'MuseuAerospacial',address:'Av Marechal Fontenele 2000',zipcode:'21740-000',latitude:'-22.884.809',longitude:'-433.919.077'})
CREATE (MuseuArquidiocesanodeArteSacra:PontoTuristico{name:'MuseuArquidiocesanodeArteSacra',address:'Av  Republica do Chile 245',zipcode:'20031-170',latitude:'-229.108.928',longitude:'-43.182.854'})
CREATE (MuseuCarpologicodoJardimBotanicodoRiodeJaneir:PontoTuristico{name:'MuseuCarpologicodoJardimBotanicodoRiodeJaneir',address:'Rua Pacheco Leao 915',zipcode:'22460-030',latitude:'-22.965.599',longitude:'-432.303.486'})
CREATE (MuseuCasadoPontal:PontoTuristico{name:'MuseuCasadoPontal',address:'Estrada do Pontal 3295',zipcode:'',latitude:'-23.024.165',longitude:'-435.123.212'})
CREATE (MuseuChacaradoCeu:PontoTuristico{name:'MuseuChacaradoCeu',address:'Rua Murtinho Nobre 93',zipcode:'20241-050',latitude:'-229.173.398',longitude:'-431.863.548'})
CREATE (MuseuHistoricoNacional:PontoTuristico{name:'MuseuHistoricoNacional',address:'Praca Mal  ancora s/n',zipcode:'20021-200',latitude:'-229.059.033',longitude:'-431.717.017'})
CREATE (MuseuMilitarCondedeLinhares:PontoTuristico{name:'MuseuMilitarCondedeLinhares',address:'Av Pedro II 383',zipcode:'20941-070',latitude:'-229.048.693',longitude:'-432.214.072'})
CREATE (MuseuNacional:PontoTuristico{name:'MuseuNacional',address:'Quinta da Boa Vista',zipcode:'20940-040',latitude:'-22.905.755',longitude:'-432.287.176'})
CREATE (MuseuNacionaldeBelasArtes:PontoTuristico{name:'MuseuNacionaldeBelasArtes',address:'Av Rio Branco 199',zipcode:'20040-008',latitude:'-22.908.755',longitude:'-431.778.894'})
CREATE (MuseudeArteModerna:PontoTuristico{name:'MuseudeArteModerna',address:'Av Infante Dom Henrique 85',zipcode:'20021-140',latitude:'-229.136.933',longitude:'-431.739.896'})
CREATE (MuseudeArteNaif:PontoTuristico{name:'MuseudeArteNaif',address:'Rua Cosme Velho 561',zipcode:'22241-090',latitude:'-22.940.241',longitude:'-432.011.867'})
CREATE (MuseudeArtedoRio:PontoTuristico{name:'MuseudeArtedoRio',address:'Praca Maua 5',zipcode:'',latitude:'-228.967.034',longitude:'-43.184.241'})
CREATE (MuseudoAmanha:PontoTuristico{name:'MuseudoAmanha',address:'Praca Maua 1',zipcode:'',latitude:'-228.939.718',longitude:'-431.816.232'})
CREATE (MuseudoAcude:PontoTuristico{name:'MuseudoAcude',address:'',zipcode:'',latitude:'-229.648.172',longitude:'-432.843.224'})
CREATE (MuseudoTrem:PontoTuristico{name:'MuseudoTrem',address:'Rua Arquias Cordeiro 1046',zipcode:'20770-001',latitude:'-2.289.508',longitude:'-432.939.273'})
CREATE (OiFuturoFlamengo:PontoTuristico{name:'OiFuturoFlamengo',address:'Rua Dois de Dezembro 63',zipcode:'22220-040',latitude:'-229.299.729',longitude:'-431.765.847'})
CREATE (OiFuturoIpanema:PontoTuristico{name:'OiFuturoIpanema',address:'Rua Visc de Piraja 54',zipcode:'22410-003',latitude:'-229.847.011',longitude:'-431.966.211'})
CREATE (PalacioGustavoCapanema:PontoTuristico{name:'PalacioGustavoCapanema',address:'Rua da Imprensa 16',zipcode:'',latitude:'-229.088.407',longitude:'-43.173.664'})
CREATE (PalacioTiradentes:PontoTuristico{name:'PalacioTiradentes',address:'Rua Primeiro de Marco s/n',zipcode:'',latitude:'-229.018.325',longitude:'-431.762.092'})
CREATE (PalaciodaJustica:PontoTuristico{name:'PalaciodaJustica',address:'Rua Dom Manuel 29',zipcode:'20010-090',latitude:'-229.046.663',longitude:'-431.725.359'})
CREATE (PalaciodoCatete:PontoTuristico{name:'PalaciodoCatete',address:'Rua do Catete 153',zipcode:'22220-000',latitude:'-229.259.954',longitude:'-431.765.801'})
CREATE (ParqueDoisIrmaos:PontoTuristico{name:'ParqueDoisIrmaos',address:'Rua Aperana 178  State of Rio de Janeiro',zipcode:'',latitude:'-229.898.943',longitude:'-432.318.343'})
CREATE (ParqueEstadualdaChacrinha:PontoTuristico{name:'ParqueEstadualdaChacrinha',address:'Rua Guimaraes Natal',zipcode:'',latitude:'-22.962.389',longitude:'-431.821.086'})
CREATE (ParqueEstadualdaPedraBranca:PontoTuristico{name:'ParqueEstadualdaPedraBranca',address:'Estrada do Pau-da-Fome 4003',zipcode:'',latitude:'-229.320.923',longitude:'-434.499.515'})
CREATE (ParqueLage:PontoTuristico{name:'ParqueLage',address:'Rua Jardim Botanico 414',zipcode:'',latitude:'-229.581.485',longitude:'-432.138.316'})
CREATE (ParqueMadureira:PontoTuristico{name:'ParqueMadureira',address:'Rua Soares Caldeira 115',zipcode:'',latitude:'-228.633.958',longitude:'-433.478.089'})
CREATE (ParqueNacionaldaTijuca:PontoTuristico{name:'ParqueNacionaldaTijuca',address:'Estrada da Cascatinha 850 - Floresta da Tijuca (Alto da Boa Vista)',zipcode:'',latitude:'-229.510.224',longitude:'-432.118.152'})
CREATE (ParqueNaturalMunicipalChicoMendes:PontoTuristico{name:'ParqueNaturalMunicipalChicoMendes',address:'Av Jarbas de Carvalho 679',zipcode:'',latitude:'-230.225.865',longitude:'-434.740.648'})
CREATE (ParqueNaturalMunicipalPaisagemCarioca:PontoTuristico{name:'ParqueNaturalMunicipalPaisagemCarioca',address:'Pca Almirante Julio de Noronha',zipcode:'',latitude:'-229.619.368',longitude:'-431.675.783'})
CREATE (ParqueNaturalMunicipaldaPrainha:PontoTuristico{name:'ParqueNaturalMunicipaldaPrainha',address:'Av Estado da Guanabara s/n',zipcode:'',latitude:'-230.366.012',longitude:'-435.125.904'})
CREATE (ParqueNaturalMunicipaldeMarapendi:PontoTuristico{name:'ParqueNaturalMunicipaldeMarapendi',address:'Av Alfredo Baltazar da Silveira s/n',zipcode:'',latitude:'-230.119.574',longitude:'-434.259.479'})
CREATE (ParqueNaturalMunicipaldoPenhascoDoisIrmaos:PontoTuristico{name:'ParqueNaturalMunicipaldoPenhascoDoisIrmaos',address:'Rua Aperana',zipcode:'',latitude:'-229.898.943',longitude:'-432.318.343'})
CREATE (ParqueOlimpico:PontoTuristico{name:'ParqueOlimpico',address:'Av Embaixador Abelardo Bueno 3401',zipcode:'22775-039',latitude:'-22.977.923',longitude:'-433.959.656'})
CREATE (ParqueRadical:PontoTuristico{name:'ParqueRadical',address:'Parque Olimpico de Deodoro - Estr. Mal. Alencastro 1357',zipcode:'',latitude:'-229.777.447',longitude:'-434.638.167'})
CREATE (ParquedaCatacumba:PontoTuristico{name:'ParquedaCatacumba',address:'Av Epitacio Pessoa 3000',zipcode:'',latitude:'-229.722.932',longitude:'-432.035.247'})
CREATE (ParquedaGavea:PontoTuristico{name:'ParquedaGavea',address:'Estrada de Santa Marinha 505',zipcode:'',latitude:'-229.801.378',longitude:'-432.414.732'})
CREATE (ParquedasRuinas:PontoTuristico{name:'ParquedasRuinas',address:'Rua Murtinho Nobre 169',zipcode:'20241-050',latitude:'-229.173.117',longitude:'-431.862.881'})
CREATE (PacoImperial:PontoTuristico{name:'PacoImperial',address:'Pca XV de Novembro 48',zipcode:'',latitude:'-229.032.009',longitude:'-43.175.207'})
CREATE (PedraBonita:PontoTuristico{name:'PedraBonita',address:'Parque Nacional da Tijuca',zipcode:'',latitude:'-229.890.024',longitude:'-43.286.433'})
CREATE (PedradaGavea:PontoTuristico{name:'PedradaGavea',address:'',zipcode:'',latitude:'-229.975.503',longitude:'-43.293.644'})
CREATE (PedradoSal:PontoTuristico{name:'PedradoSal',address:'Largo do Sao Francisco da Prainha s/n - Saude',zipcode:'',latitude:'-22.898.037',longitude:'-43.187.849'})
CREATE (PercursodoBondedeSantaTeresa:PontoTuristico{name:'PercursodoBondedeSantaTeresa',address:'',zipcode:'',latitude:'-22.910.278',longitude:'-43.178.889'})
CREATE (PercursodoVLT:PontoTuristico{name:'PercursodoVLT',address:'',zipcode:'',latitude:'-229.108.116',longitude:'-431.772.158'})
CREATE (PlanetariodaGavea:PontoTuristico{name:'PlanetariodaGavea',address:'Rua Vice-Governador Rubens Berardo 100',zipcode:'22451-070',latitude:'-22.978.242',longitude:'-432.324.218'})
CREATE (PraiaVermelha:PontoTuristico{name:'PraiaVermelha',address:'Praca General Tiburcio',zipcode:'22290-270',latitude:'-229.552.279',longitude:'-431.669.132'})
CREATE (PraiadaBarradaTijuca:PontoTuristico{name:'PraiadaBarradaTijuca',address:'',zipcode:'',latitude:'-23.010.833',longitude:'-43.359.722'})
CREATE (PraiadaJoatinga:PontoTuristico{name:'PraiadaJoatinga',address:'',zipcode:'',latitude:'-23.014.722',longitude:'-43.290.556'})
CREATE (PraiadaMacumba:PontoTuristico{name:'PraiadaMacumba',address:'',zipcode:'',latitude:'-230.330.506',longitude:'-434.863.554'})
CREATE (PraiadaPrainha:PontoTuristico{name:'PraiadaPrainha',address:'',zipcode:'',latitude:'-230.405.506',longitude:'-435.074.665'})
CREATE (PraiadaReserva:PontoTuristico{name:'PraiadaReserva',address:'',zipcode:'',latitude:'-23.012.495',longitude:'-433.907.998'})
CREATE (PraiadeAbrico:PontoTuristico{name:'PraiadeAbrico',address:'',zipcode:'',latitude:'-230.480.506',longitude:'-43.515.522'})
CREATE (PraiadeCopacabana:PontoTuristico{name:'PraiadeCopacabana',address:'',zipcode:'',latitude:'-229.708.283',longitude:'-431.841.331'})
CREATE (PraiadeGrumari:PontoTuristico{name:'PraiadeGrumari',address:'',zipcode:'',latitude:'-230.485.259',longitude:'-435.256.342'})
CREATE (PraiadeIpanema:PontoTuristico{name:'PraiadeIpanema',address:'',zipcode:'',latitude:'-229.869.394',longitude:'-432.063.554'})
CREATE (PraiadeSaoConrado:PontoTuristico{name:'PraiadeSaoConrado',address:'',zipcode:'',latitude:'-22.999.995',longitude:'-432.696.887'})
CREATE (PraiadoArpoador:PontoTuristico{name:'PraiadoArpoador',address:'Entre o Forte de Copacabana e a Rua Francisco Otaviano com Av Vieira Souto',zipcode:'',latitude:'-229.885.617',longitude:'-431.951.814'})
CREATE (PraiadoDiabo:PontoTuristico{name:'PraiadoDiabo',address:'Esquerda da Pedra do Arpoador',zipcode:'',latitude:'-229.883.716',longitude:'-431.915.257'})
CREATE (PraiadoLeblon:PontoTuristico{name:'PraiadoLeblon',address:'Av Delfim Moreira',zipcode:'',latitude:'-229.877.717',longitude:'-432.236.142'})
CREATE (PraiadoLeme:PontoTuristico{name:'PraiadoLeme',address:'',zipcode:'',latitude:'-229.631.205',longitude:'-43.170.973'})
CREATE (PraiadoPepe:PontoTuristico{name:'PraiadoPepe',address:'Avenida Pepe S/N',zipcode:'22620-170',latitude:'-230.149.534',longitude:'-433.156.156'})
CREATE (PraiadoPontal:PontoTuristico{name:'PraiadoPontal',address:'',zipcode:'',latitude:'-23.031.495',longitude:'-43.470.262'})
CREATE (PraiadoRecreio:PontoTuristico{name:'PraiadoRecreio',address:'',zipcode:'',latitude:'-230.265',longitude:'-434.591.944.444'})
CREATE (Praiasselvagensmeiofundarasaeperigoso:PontoTuristico{name:'Praiasselvagensmeiofundarasaeperigoso',address:'',zipcode:'',latitude:'-230.717.222.222',longitude:'-435.524.444.444'})
CREATE (PracaMaua:PontoTuristico{name:'PracaMaua',address:'Av Rodrigues Alves 135',zipcode:'',latitude:'-228.962.339',longitude:'-431.831.547'})
CREATE (PracaParis:PontoTuristico{name:'PracaParis',address:'Av Augusto Severo 342',zipcode:'20021-040',latitude:'-229.158.591',longitude:'-43.177.957'})
CREATE (PracaTiradentes:PontoTuristico{name:'PracaTiradentes',address:'Praca Tiradentes',zipcode:'20060-010',latitude:'-228.962.339',longitude:'-431.831.547'})
CREATE (PracaXV:PontoTuristico{name:'PracaXV',address:'Praca Quinze de Novembro s/n',zipcode:'20010-000',latitude:'-22.902.905',longitude:'-431.758.521'})
CREATE (PaodeAcucar:PontoTuristico{name:'PaodeAcucar',address:'',zipcode:'',latitude:'-229.492.383',longitude:'-431.633.305'})
CREATE (PoloGastronomicoRuaNelsonMandela:PontoTuristico{name:'PoloGastronomicoRuaNelsonMandela',address:'',zipcode:'',latitude:'-22.949.722',longitude:'-43.184.722'})
CREATE (QuintadaBoaVista:PontoTuristico{name:'QuintadaBoaVista',address:'Av Pedro II s/n',zipcode:'20940-040',latitude:'-229.053.673',longitude:'-432.256.757'})
CREATE (RealGabinetePortuguesdeLeitura:PontoTuristico{name:'RealGabinetePortuguesdeLeitura',address:'R. Luis de Camoes 30',zipcode:'20051-020',latitude:'-229.054.057',longitude:'-431.843.975'})
CREATE (RestingadaMarambaia:PontoTuristico{name:'RestingadaMarambaia',address:'',zipcode:'',latitude:'-230.439.722.222',longitude:'-436.446.111.111'})
CREATE (Riocentro:PontoTuristico{name:'Riocentro',address:'Av Salvador Allende 6555',zipcode:'22783-127',latitude:'-229.785.251',longitude:'-434.138.596'})
CREATE (SalaCeciliaMeirelles:PontoTuristico{name:'SalaCeciliaMeirelles',address:'Rua da Lapa 47',zipcode:'20021-180',latitude:'-229.144.872',longitude:'-431.806.154'})
CREATE (Sambodromo:PontoTuristico{name:'Sambodromo',address:'Rua Marquês de Sapucai - Santo Cristo',zipcode:'20220-007',latitude:'-229.114.472',longitude:'-431.989.927'})
CREATE (SantaTeresaTP:PontoTuristico{name:'SantaTeresa',address:'Rua Joaquim Murtinho 13190',zipcode:'20241-320',latitude:'-229.189.469',longitude:'-431.854.071'})
CREATE (SitioBurleMarx:PontoTuristico{name:'SitioBurleMarx',address:'Estrada Roberto Burle Marx (anitga Estrada da Barra de Guaratiba) 2019',zipcode:'',latitude:'-230.222.851',longitude:'-435.483.909'})
CREATE (TeatroPopular:PontoTuristico{name:'TeatroPopular',address:'Av Plinio Leite s/n - Caminho Niemeyer',zipcode:'',latitude:'-228.880.681',longitude:'-431.298.471'})
CREATE (TheatroMunicipal:PontoTuristico{name:'TheatroMunicipal',address:'Praca Floriano s/n',zipcode:'',latitude:'-229.090.431',longitude:'-431.787.462'})
CREATE (TremdoCorcovado:PontoTuristico{name:'TremdoCorcovado',address:'Rua Cosme Velho 513',zipcode:'22241-091',latitude:'-229.406.018',longitude:'-432.007.701'})
CREATE (UrcaTP:PontoTuristico{name:'UrcaTP',address:'',zipcode:'',latitude:'-2.295.525',longitude:'-431.670.555.556'})
CREATE (ViadutodeMadureira:PontoTuristico{name:'ViadutodeMadureira',address:'Rua Carvalho de Souza s/n',zipcode:'',latitude:'-22.876.038',longitude:'-433.369.597'})
CREATE (ViladosAtletas:PontoTuristico{name:'ViladosAtletas',address:'Av Salvador Allende 3200',zipcode:'22780-160',latitude:'-229.649.921',longitude:'-433.973.627'})
CREATE (VistaChinesa:PontoTuristico{name:'VistaChinesa',address:'Estrada da Vista Chinesa 1294 - Alto da Boa Vista',zipcode:'',latitude:'-229.732.372',longitude:'-432.516.327'})

// Create Bairros
CREATE (Centro:Bairro{name:'Centro'})
CREATE (Lapa:Bairro{name:'Lapa'})
CREATE (Gloria:Bairro{name:'Gloria'})
CREATE (Botafogo:Bairro{name:'Botafogo'})
CREATE (Copacabana:Bairro{name:'Copacabana'})
CREATE (Laranjeiras:Bairro{name:'Laranjeiras'})
CREATE (SantaTeresa:Bairro{name:'SantaTeresa'})
CREATE (Pechincha:Bairro{name:'Pechincha'})
CREATE (Tijuca:Bairro{name:'Tijuca'})
CREATE (Benfica:Bairro{name:'Benfica'})
CREATE (Lagoa:Bairro{name:'Lagoa'})
CREATE (Urca:Bairro{name:'Urca'})
CREATE (MariadaGraca:Bairro{name:'MariadaGraca'})
CREATE (Leblon:Bairro{name:'Leblon'})
CREATE (Flamengo:Bairro{name:'Flamengo'})
CREATE (BarradaTijuca:Bairro{name:'BarradaTijuca'})
CREATE (Niteroi:Bairro{name:'Niteroi'})
CREATE (Madureira:Bairro{name:'Madureira'})
CREATE (EngenhodeDentro:Bairro{name:'EngenhodeDentro'})
CREATE (OswaldoCruz:Bairro{name:'OswaldoCruz'})
CREATE (SaoCristovao:Bairro{name:'SaoCristovao'})
CREATE (Leme:Bairro{name:'Leme'})
CREATE (Ramos:Bairro{name:'Ramos'})
CREATE (Andarai:Bairro{name:'Andarai'})
CREATE (CidadeNova:Bairro{name:'CidadeNova'})
CREATE (VilaIsabel:Bairro{name:'VilaIsabel'})
CREATE (Horto:Bairro{name:'Horto'})
CREATE (JardimBotanico:Bairro{name:'JardimBotanico'})
CREATE (Gavea:Bairro{name:'Gavea'})
CREATE (Maracana:Bairro{name:'Maracana'})
CREATE (SaoConrado:Bairro{name:'SaoConrado'})
CREATE (CampodosAfonsos:Bairro{name:'CampodosAfonsos'})
CREATE (Recreio:Bairro{name:'Recreio'})
CREATE (CosmeVelho:Bairro{name:'CosmeVelho'})
CREATE (Ipanema:Bairro{name:'Ipanema'})
CREATE (Catete:Bairro{name:'Catete'})
CREATE (Taquara:Bairro{name:'Taquara'})
CREATE (RicardodeAlbuquerque:Bairro{name:'RicardodeAlbuquerque'})

// Create Municipios
CREATE (RioDeJaneiro:Municipio{name:'Rio de Janeiro'})
CREATE (DuqueDeCaxias:Municipio{name:'Duque De Caxias'})
CREATE (SaoJoaoDoMeriti:Municipio{name:'Sao Joao do Meriti'})
CREATE (Saquarema:Municipio{name:'Saquarema'})

// Create Estado
CREATE (RJ:Estado{name:'RJ'})

// Create Relações para Grafos
CREATE (AquaRio)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(ArcodoTeles)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(ArcosdaLapa)-[:PERTENCE {roles:['Pertence']}]->(Lapa),
(AterrodoFlamengo)-[:PERTENCE {roles:['Pertence']}]->(Gloria),
(BarTombadoAdegaDaVelha)-[:PERTENCE {roles:['Pertence']}]->(Botafogo),
(BarTombadoAdegaFlorDeCoimbra)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoAdegaPerola)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(BarTombadoArmazemCardosao)-[:PERTENCE {roles:['Pertence']}]->(Laranjeiras),
(BarTombadoArmazemDoSenado)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoArmazemSaoThiagoBarDoGomez)-[:PERTENCE {roles:['Pertence']}]->(SantaTeresa),
(BarTombadoBarBrasil)-[:PERTENCE {roles:['Pertence']}]->(Pechincha),
(BarTombadoBarDaDonaMariaCafeEBarBrotinh)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(BarTombadoBarDoAdonis)-[:PERTENCE {roles:['Pertence']}]->(Benfica),
(BarTombadoBarLagoa)-[:PERTENCE {roles:['Pertence']}]->(Lagoa),
(BarTombadoBarLuiz)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoBarUrca)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(BarTombadoBipBip)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(BarTombadoBotequimDoJoia)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoCafeEBarLisbela)-[:PERTENCE {roles:['Pertence']}]->(MariadaGraca),
(BarTombadoCasaDaCachaca)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoCasaPaladino)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoCasaVilarino)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoCervantes)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(BarTombadoCosmopolita)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoJobi)-[:PERTENCE {roles:['Pertence']}]->(Leblon),
(BarTombadoLamas)-[:PERTENCE {roles:['Pertence']}]->(Flamengo),
(BarTombadoNovaCapela)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BarTombadoPavaoAzul)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(BarTombadoRestauranteSalete)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(BibliotecaNacional)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BibliotecaParqueEstadual)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(BosquedaBarra)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(CADEG)-[:PERTENCE {roles:['Pertence']}]->(Benfica),
(CRAB)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CaisdoValongo)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CaminhoNiemyer)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(CampoOlimpicodeGolfe)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(CampodeSantana)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CasaFrancaBrasil)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CasaHistoricadeDeodoro)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CasadoJongo)-[:PERTENCE {roles:['Pertence']}]->(Madureira),
(CentroCulturalBancodoBrasil)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CentroCulturalJoseBonifacio)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CentroCulturalJusticaFederal)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CentroCulturaldosCorreios)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(CentrodeMemoriaRobertoSilveira)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(ChacaraTropical)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(CidadedasArtes)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(CinelandiaPracaFloriano)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(CristoRedentor)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(EscadariaSelaron)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(EspacoCienciaViva)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(EstacaoHidroviariadeCharitas)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(EstradadasPaineiras)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(EstadioOlimpicoEngenhao)-[:PERTENCE {roles:['Pertence']}]->(EngenhodeDentro),
(FeiradasYabas)-[:PERTENCE {roles:['Pertence']}]->(OswaldoCruz),
(FeiradeSaoCristovao)-[:PERTENCE {roles:['Pertence']}]->(SaoCristovao),
(FlorestadaTijuca)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(FortalezaNossaSenhoradaConceicao)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(FortalezadeSaoJoao)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(ForteDuquedeCaxiasPedradoLeme)-[:PERTENCE {roles:['Pertence']}]->(Leme),
(FortedeCopacabana)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(FundacaoCasadeRuiBarbosa)-[:PERTENCE {roles:['Pertence']}]->(Botafogo),
(FundacaoOscarNiemeyer)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(FabricaBhering)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(GRESImperatrizLeopoldinense)-[:PERTENCE {roles:['Pertence']}]->(Ramos),
(GRESImperioSerrano)-[:PERTENCE {roles:['Pertence']}]->(Madureira),
(GRESMangueira)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(GRESPortela)-[:PERTENCE {roles:['Pertence']}]->(OswaldoCruz),
(GRESSalgueiro)-[:PERTENCE {roles:['Pertence']}]->(Andarai),
(GRESSaoClemente)-[:PERTENCE {roles:['Pertence']}]->(CidadeNova),
(GRESVilaIsabel)-[:PERTENCE {roles:['Pertence']}]->(VilaIsabel),
(GalpaoGamboa)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(HortoFlorestal)-[:PERTENCE {roles:['Pertence']}]->(Horto),
(IlhaFiscal)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(IlhadePaqueta)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(JardimBotanicoTP)-[:PERTENCE {roles:['Pertence']}]->(JardimBotanico),
(JardinsSuspensosdoValongo)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(JockeyClub)-[:PERTENCE {roles:['Pertence']}]->(Gavea),
(LagoaRodrigodeFreitas)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(LargodaCariocaCaixaCultural)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MACMuseudeArteContemporanea)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MaracanaTP)-[:PERTENCE {roles:['Pertence']}]->(Maracana),
(MarinadaGloria)-[:PERTENCE {roles:['Pertence']}]->(Gloria),
(MemorialdosPretosNovos)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MercadaodeMadureira)-[:PERTENCE {roles:['Pertence']}]->(Madureira),
(MesadoImperador)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MiranteDonaMarta)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MirantedaPrainha)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(MirantedoLeblon)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MirantedoPasmado)-[:PERTENCE {roles:['Pertence']}]->(Botafogo),
(MonumentoEstaciodeSa)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MorroDoisIrmaos)-[:PERTENCE {roles:['Pertence']}]->(SaoConrado),
(MorrodaUrca)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(MuseuAerospacial)-[:PERTENCE {roles:['Pertence']}]->(CampodosAfonsos),
(MuseuArquidiocesanodeArteSacra)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MuseuCarpologicodoJardimBotanicodoRiodeJaneir)-[:PERTENCE {roles:['Pertence']}]->(JardimBotanico),
(MuseuCasadoPontal)-[:PERTENCE {roles:['Pertence']}]->(Recreio),
(MuseuChacaradoCeu)-[:PERTENCE {roles:['Pertence']}]->(SantaTeresa),
(MuseuHistoricoNacional)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MuseuMilitarCondedeLinhares)-[:PERTENCE {roles:['Pertence']}]->(SaoCristovao),
(MuseuNacional)-[:PERTENCE {roles:['Pertence']}]->(SaoCristovao),
(MuseuNacionaldeBelasArtes)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MuseudeArteModerna)-[:PERTENCE {roles:['Pertence']}]->(Flamengo),
(MuseudeArteNaif)-[:PERTENCE {roles:['Pertence']}]->(CosmeVelho),
(MuseudeArtedoRio)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MuseudoAmanha)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(MuseudoAcude)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(MuseudoTrem)-[:PERTENCE {roles:['Pertence']}]->(EngenhodeDentro),
(OiFuturoFlamengo)-[:PERTENCE {roles:['Pertence']}]->(Flamengo),
(OiFuturoIpanema)-[:PERTENCE {roles:['Pertence']}]->(Ipanema),
(PalacioGustavoCapanema)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PalacioTiradentes)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PalaciodaJustica)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PalaciodoCatete)-[:PERTENCE {roles:['Pertence']}]->(Catete),
(ParqueDoisIrmaos)-[:PERTENCE {roles:['Pertence']}]->(Leblon),
(ParqueEstadualdaChacrinha)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(ParqueEstadualdaPedraBranca)-[:PERTENCE {roles:['Pertence']}]->(Taquara),
(ParqueLage)-[:PERTENCE {roles:['Pertence']}]->(JardimBotanico),
(ParqueMadureira)-[:PERTENCE {roles:['Pertence']}]->(Madureira),
(ParqueNacionaldaTijuca)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(ParqueNaturalMunicipalChicoMendes)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(ParqueNaturalMunicipalPaisagemCarioca)-[:PERTENCE {roles:['Pertence']}]->(Leme),
(ParqueNaturalMunicipaldaPrainha)-[:PERTENCE {roles:['Pertence']}]->(Recreio),
(ParqueNaturalMunicipaldeMarapendi)-[:PERTENCE {roles:['Pertence']}]->(Recreio),
(ParqueNaturalMunicipaldoPenhascoDoisIrmaos)-[:PERTENCE {roles:['Pertence']}]->(Leblon),
(ParqueOlimpico)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(ParqueRadical)-[:PERTENCE {roles:['Pertence']}]->(RicardodeAlbuquerque),
(ParquedaCatacumba)-[:PERTENCE {roles:['Pertence']}]->(Lagoa),
(ParquedaGavea)-[:PERTENCE {roles:['Pertence']}]->(Gavea),
(ParquedasRuinas)-[:PERTENCE {roles:['Pertence']}]->(SantaTeresa),
(PacoImperial)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PedraBonita)-[:PERTENCE {roles:['Pertence']}]->(Tijuca),
(PedradaGavea)-[:PERTENCE {roles:['Pertence']}]->(Gavea),
(PedradoSal)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PercursodoBondedeSantaTeresa)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PercursodoVLT)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PlanetariodaGavea)-[:PERTENCE {roles:['Pertence']}]->(Gavea),
(PraiaVermelha)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(PraiadaBarradaTijuca)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(PraiadaJoatinga)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadaMacumba)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadaPrainha)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadaReserva)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadeAbrico)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadeCopacabana)-[:PERTENCE {roles:['Pertence']}]->(Copacabana),
(PraiadeGrumari)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadeIpanema)-[:PERTENCE {roles:['Pertence']}]->(Ipanema),
(PraiadeSaoConrado)-[:PERTENCE {roles:['Pertence']}]->(SaoConrado),
(PraiadoArpoador)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadoDiabo)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadoLeblon)-[:PERTENCE {roles:['Pertence']}]->(Leblon),
(PraiadoLeme)-[:PERTENCE {roles:['Pertence']}]->(Leme),
(PraiadoPepe)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(PraiadoPontal)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PraiadoRecreio)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(Praiasselvagensmeiofundarasaeperigoso)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(PracaMaua)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PracaParis)-[:PERTENCE {roles:['Pertence']}]->(Gloria),
(PracaTiradentes)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PracaXV)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(PaodeAcucar)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(PoloGastronomicoRuaNelsonMandela)-[:PERTENCE {roles:['Pertence']}]->(Botafogo),
(QuintadaBoaVista)-[:PERTENCE {roles:['Pertence']}]->(SaoCristovao),
(RealGabinetePortuguesdeLeitura)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(RestingadaMarambaia)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(Riocentro)-[:PERTENCE {roles:['Pertence']}]->(BarradaTijuca),
(SalaCeciliaMeirelles)-[:PERTENCE {roles:['Pertence']}]->(Lapa),
(Sambodromo)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(SantaTeresaTP)-[:PERTENCE {roles:['Pertence']}]->(SantaTeresa),
(SitioBurleMarx)-[:PERTENCE {roles:['Pertence']}]->(Niteroi),
(TeatroPopular)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(TheatroMunicipal)-[:PERTENCE {roles:['Pertence']}]->(Centro),
(TremdoCorcovado)-[:PERTENCE {roles:['Pertence']}]->(CosmeVelho),
(UrcaTP)-[:PERTENCE {roles:['Pertence']}]->(Urca),
(ViadutodeMadureira)-[:PERTENCE {roles:['Pertence']}]->(Madureira),
(ViladosAtletas)-[:PERTENCE {roles:['Pertence']}]->(Recreio),
(VistaChinesa)-[:PERTENCE {roles:['Pertence']}]->(Tijuca)

// Create relacionamento Bairro -> Municipios
CREATE
(Gloria)-[:PERTENCE {roles:['Pertence']}]->(DuqueDeCaxias),
(Botafogo)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Copacabana)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Laranjeiras)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(SantaTeresa)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Pechincha)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Tijuca)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Benfica)-[:PERTENCE {roles:['Pertence']}]->(SaoJoaoDoMeriti),
(Lagoa)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Urca)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(MariadaGraca)-[:PERTENCE {roles:['Pertence']}]->(Saquarema),
(Leblon)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Flamengo)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(BarradaTijuca)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Niteroi)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Madureira)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(EngenhodeDentro)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(OswaldoCruz)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(SaoCristovao)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Leme)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Ramos)-[:PERTENCE {roles:['Pertence']}]->(SaoJoaoDoMeriti),
(Andarai)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(CidadeNova)-[:PERTENCE {roles:['Pertence']}]->(DuqueDeCaxias),
(VilaIsabel)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Horto)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(JardimBotanico)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Gavea)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Maracana)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(SaoConrado)-[:PERTENCE {roles:['Pertence']}]->(SaoJoaoDoMeriti),
(CampodosAfonsos)-[:PERTENCE {roles:['Pertence']}]->(SaoJoaoDoMeriti),
(Recreio)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(CosmeVelho)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Ipanema)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Catete)-[:PERTENCE {roles:['Pertence']}]->(DuqueDeCaxias),
(Taquara)-[:PERTENCE {roles:['Pertence']}]->(DuqueDeCaxias),
(RicardodeAlbuquerque)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Lapa)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro),
(Centro)-[:PERTENCE {roles:['Pertence']}]->(RioDeJaneiro)

// Create relacionamento Estado -> Municipios
CREATE
(RioDeJaneiro)-[:PERTENCE {roles:['Pertence']}]->(RJ),
(DuqueDeCaxias)-[:PERTENCE {roles:['Pertence']}]->(RJ),
(SaoJoaoDoMeriti)-[:PERTENCE {roles:['Pertence']}]->(RJ),
(Saquarema)-[:PERTENCE {roles:['Pertence']}]->(RJ)
"""


gdb = GraphDatabase(
    url="http://127.0.0.1:7474",
    username='neo4j1',
    password='neo4j'
)

gdb.query(data)
