from django.urls import path

from .views import ListPontosTuristicos

urlpatterns = [
    path('bairro/<str:bairro>/', ListPontosTuristicos.as_view())
]
