from rest_framework.response import Response
from rest_framework.views import APIView

from .helpers import get_pontos_turisticos


class ListPontosTuristicos(APIView):
    """docstring for ListPontosTuristicos"""

    def get(self, request, bairro, format=None):
        response = get_pontos_turisticos(bairro)

        return Response(response)
