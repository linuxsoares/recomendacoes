from neo4jrestclient.client import GraphDatabase, Node

gdb = GraphDatabase(
    url="http://127.0.0.1:7474",
    username='neo4j1',
    password='neo4j'
)


def get_pontos_turisticos(bairro):
    query = 'MATCH (n:Bairro)'\
            'WHERE n.name =~ {name}'\
            'OPTIONAL MATCH (n)-[r]-(b)'\
            'RETURN  b'
    response = gdb.query(
        query,
        returns=Node,
        params={"name": "(?i).*" + bairro + ".*"}
    )

    points = list()
    for row in response:
        for r in row:
            if r.properties['name'] != 'Rio de Janeiro':
                points.append({'ponto_turistico': r.properties})

    return points
