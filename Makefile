
MANAGE_PY=manage.py
DEV_SETTINGS=recomendacoes.settings.development

clean:
	@find . -name "*.pyc" | xargs rm -rf
	@find . -name "*.pyo" | xargs rm -rf
	@find . -name "__pycache__" -type d | xargs rm -rf
	@rm -f .coverage
	@rm -rf htmlcov/
	@rm -f coverage.xml
	@rm -f *.log

shell:
	@python $(MANAGE_PY) shell --settings=$(DEV_SETTINGS)

test: clean
	@py.test -x -p no:sugar recomendacoes

test-debug: clean
	@py.test --pdb -x -p no:sugar recomendacoes

test-matching: clean
	@py.test -rxs -k $(Q) --pdb recomendacoes

flake8:
	@flake8 --show-source .

check-python-import:
	@isort --check

fix-python-import:
	@isort -rc .

migrate:
	@python $(MANAGE_PY) migrate $(APP) --settings=$(DEV_SETTINGS)

superuser:
	@python $(MANAGE_PY) createsuperuser $(APP) --settings=$(DEV_SETTINGS)

makemigrations-dev:
	@python $(MANAGE_PY) makemigrations $(APP) --settings=$(DEV_SETTINGS)

migrate-dev:
	@python $(MANAGE_PY) migrate --settings=$(DEV_SETTINGS)

runserver-dev:
	@python $(MANAGE_PY) runserver --settings=$(DEV_SETTINGS)




