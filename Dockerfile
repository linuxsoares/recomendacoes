FROM ubuntu:latest

RUN apt-get update \
  && apt-get install -y python3-pip python3-dev git wget default-jre\
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

# FROM python:3.6.3

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD . /code/
RUN pip install -r requirements/development.txt
RUN pip install git+https://github.com/Supervisor/supervisor@master
RUN pip install gunicorn
ADD . /code/

ADD supervisord.conf /etc/supervisor/conf.d/my_app.conf

RUN mkdir logs
RUN touch logs/supervisor.log


# neo4j

# FROM frodenas/java7

# Install and configure Neo4j 2.1.2
RUN cd /tmp && \
    wget http://dist.neo4j.org/neo4j-community-2.1.2-unix.tar.gz && \
    tar xzvf neo4j-community-2.1.2-unix.tar.gz && \
    mv /tmp/neo4j-community-2.1.2/ /neo4j && \
    sed -e 's/^org.neo4j.server.database.location=.*$/org.neo4j.server.database.location=\/data\/graph.db/' -i /neo4j/conf/neo4j-server.properties && \
    sed -e 's/^#org.neo4j.server.webserver.address=.*$/org.neo4j.server.webserver.address=0.0.0.0/' -i /neo4j/conf/neo4j-server.properties && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add scripts
RUN chmod +x neo4j/*.sh


# Expose listen port
EXPOSE 7474 8000

# Expose our data volumes
VOLUME ["/data"]

# Command to run
RUN neo4j/run.sh

# Run django
CMD ["supervisord"]